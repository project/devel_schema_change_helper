<?php

namespace Drupal\devel_schema_change_helper\Drush\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageException;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldStorageConfigInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains "Devel Schema Change Helper" drush commands.
 */
final class DevelSchemaChangeHelperCommands extends DrushCommands {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity definition update manager service.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a DevelSchemaChangeHelperCommands object.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager,
    Connection $database
    ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.definition_update_manager'),
      $container->get('database'),
    );
  }

  /**
   * Command description here.
   *
   * NOTE, this DOES NOT SUPPORT the "entity_reference" fields!
   */
  #[CLI\Command(name: 'devel_schema_change_helper:rename-existing-field', aliases: ['dsch-rename-field'])]
  #[CLI\Argument(name: 'entityTypeId', description: 'The entity type id the field exists on (e.g. "media")')]
  #[CLI\Argument(name: 'oldFieldName', description: 'The old field name.')]
  #[CLI\Argument(name: 'newFieldName', description: 'The new field name.')]
  #[CLI\Argument(name: 'deleteOldFieldData', description: '(Optional) Whether to delete the old field data (Defaults to FALSE)')]
  #[CLI\Usage(name: 'dsch-rename-field node old_field_name new_field_name TRUE', description: '=> Rename the field "old_field_name" to "new_field_name" on the entity type node, deleting the old field definition.')]
  #[CLI\Help(description: 'Rename an existing field on an entity type. NOTE, this DOES NOT SUPPORT the "entity_reference" fields!')]
  public function renameExistingField(string $entityTypeId, string $oldFieldName, string $newFieldName, bool $deleteOldFieldData = FALSE) {
    // Get the old field storage definition and build our new field storage
    // definition from it:
    $oldFieldStorageDefinition = FieldStorageConfig::loadByName($entityTypeId, $oldFieldName);
    $newFieldStorageDefinition = $this->copyFieldStorage($oldFieldStorageDefinition, $newFieldName);
    // Copy over the field config:
    $this->copyFieldConfig($oldFieldStorageDefinition, $oldFieldName, $newFieldName);

    // Copy over the field view and form displays:
    $this->copyViewAndFormDisplays($oldFieldStorageDefinition, $oldFieldName, $newFieldName);

    // Create the field table mapping array:
    $fieldTablesMappingArray = $this->createFieldTablesMappingArray($oldFieldStorageDefinition, $newFieldStorageDefinition);

    // Convert field table data to be used by the new field:
    $convertedFieldTableData = $this->convertFieldTableData($fieldTablesMappingArray, $oldFieldName, $newFieldName);

    // Update the field storage definition:
    $this->entityDefinitionUpdateManager->updateFieldStorageDefinition($oldFieldStorageDefinition);

    // Copy the db field data from the old field to the new field:
    $this->copyFieldTableData($convertedFieldTableData);

    // If the old field is a media field, see if it is used as a source field:
    if ($entityTypeId === 'media') {
      $this->changeSourceConfigurationOnExistingMediaTypes($oldFieldName, $newFieldName);
    }

    // Delete the old field storage. This will automatically remove the field
    // config and the database entries as well!
    if ($deleteOldFieldData) {
      $oldFieldStorageDefinition->delete();
    }

    $this->io()->success('Field renamed successfully!');
  }

  /**
   * Makes a copy of an existing field storage.
   *
   * @param \Drupal\field\FieldStorageConfigInterface $oldFieldStorage
   *   The old field storage definition.
   * @param string $newFieldName
   *   The name of the new field name the storage belongs to.
   *
   * @return \Drupal\field\FieldStorageConfigInterface
   *   The new field storage definition.
   */
  protected function copyFieldStorage(FieldStorageConfigInterface $oldFieldStorage, string $newFieldName): FieldStorageConfigInterface {
    $newFieldStorageDefintionValues = $oldFieldStorage->toArray();
    unset($newFieldStorageDefintionValues['uuid']);
    unset($newFieldStorageDefintionValues['_core']);
    $newFieldStorageDefintionValues['field_name'] = $newFieldName;
    $newFieldStorageDefintionValues['id'] = $oldFieldStorage->getTargetEntityTypeId() . '.' . $newFieldName;
    // Create the new field storage with the adjusted values of the old field
    // storage:
    $newFieldStorageDefinition = FieldStorageConfig::create($newFieldStorageDefintionValues);
    $newFieldStorageDefinition->save();
    return $newFieldStorageDefinition;
  }

  /**
   * Copy the field config for each given bundle.
   *
   * @param \Drupal\field\FieldStorageConfigInterface $oldFieldStorage
   *   The old field storage definition.
   * @param string $oldFieldName
   *   The old field name.
   * @param string $newFieldName
   *   The new field name.
   */
  protected function copyFieldConfig(FieldStorageConfigInterface $oldFieldStorage, string $oldFieldName, string $newFieldName): void {
    foreach ($oldFieldStorage->getBundles() as $bundle) {
      $oldField = FieldConfig::loadByName($oldFieldStorage->getTargetEntityTypeId(), $bundle, $oldFieldName);
      $newFieldValues = $oldField->toArray();
      // Remove the old uuid and _core entry and rename the field and field id:
      unset($newFieldValues['uuid']);
      unset($newFieldValues['_core']);
      $newFieldValues['field_name'] = $newFieldName;
      $newFieldValues['id'] = $oldFieldStorage->getTargetEntityTypeId() . '.' . $newFieldValues['field_type'] . '.' . $newFieldName;

      FieldConfig::create($newFieldValues)->save();
    }
  }

  /**
   * Copy old field displays over to the new field.
   *
   * @param \Drupal\field\FieldStorageConfigInterface $oldFieldStorage
   *   The old field storage definition.
   * @param string $oldFieldName
   *   The old field name.
   * @param string $newFieldName
   *   The new field name.
   */
  protected function copyViewAndFormDisplays(FieldStorageConfigInterface $oldFieldStorage, string $oldFieldName, string $newFieldName): void {
    foreach ($oldFieldStorage->getBundles() as $bundle) {
      // Apply old field view displays on new field:
      /** @var  \Drupal\Core\Entity\Display\EntityViewDisplayInterface[] $viewDisplays */
      $viewDisplays = $this->entityTypeManager->getStorage('entity_view_display')->loadByProperties([
        'targetEntityType' => $oldFieldStorage->getTargetEntityTypeId(),
        'bundle' => $bundle,
      ]);
      foreach ($viewDisplays as $viewDisplay) {
        $oldFieldComponent = $viewDisplay->getComponent($oldFieldName);
        if (!empty($oldFieldComponent)) {
          $viewDisplay->setComponent($newFieldName, $oldFieldComponent)
            ->save();
        }
      }
      // Apply old field form displays on new field:
      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface[] $formDisplays*/
      $formDisplays = $this->entityTypeManager->getStorage('entity_form_display')->loadByProperties([
        'targetEntityType' => $oldFieldStorage->getTargetEntityTypeId(),
        'bundle' => $bundle,
      ]);
      foreach ($formDisplays as $formDisplay) {
        $oldFieldComponent = $formDisplay->getComponent($oldFieldName);
        if (!empty($oldFieldComponent)) {
          $formDisplay->setComponent($newFieldName, $oldFieldComponent)
            ->save();
        }
      }
    }
  }

  /**
   * Create a field tables mapping array.
   *
   * This method creates an array that maps the field tables from the old field
   * storage definition to the new field storage definition. The array contains
   * the old table names as keys and the corresponding new table names as
   * values.
   *
   * @param \Drupal\field\Entity\FieldStorageConfig $oldFieldStorageDefinition
   *   The old field storage definition.
   * @param \Drupal\field\Entity\FieldStorageConfig $newFieldStorageDefinition
   *   The new field storage definition.
   *
   * @return array
   *   An array that maps the field table names from the old field storage
   *   definition to the new field storage definition.
   */
  protected function createFieldTablesMappingArray(FieldStorageConfig $oldFieldStorageDefinition, FieldStorageConfig $newFieldStorageDefinition): array {
    $tableMapping = $this->entityTypeManager
      ->getStorage($oldFieldStorageDefinition->getTargetEntityTypeId())
      ->getTableMapping();
    $fieldTablesMappingArray = [];

    try {
      $oldFieldTable = $tableMapping->getFieldTableName($oldFieldStorageDefinition->getName());
      $newFieldTable = $tableMapping->getFieldTableName($newFieldStorageDefinition->getName());
    }
    catch (SqlContentEntityStorageException $e) {
      // Custom storage? Broken site? No matter what, if there is no table,
      // there's little we can do.
      $this->io()->error('The given field names could not get resolved to an existing database table.');
      throw $e;
    }

    // Add the field table names in our mapping array:
    $fieldTablesMappingArray[$oldFieldTable] = $newFieldTable;

    // See if the field has a revision table.
    $entityType = $this->entityTypeManager->getDefinition($oldFieldStorageDefinition->getTargetEntityTypeId());
    if ($entityType->isRevisionable() && $oldFieldStorageDefinition->isRevisionable()) {
      if ($tableMapping->requiresDedicatedTableStorage($oldFieldStorageDefinition)) {
        $oldFieldRevisionTable = $tableMapping->getDedicatedRevisionTableName($oldFieldStorageDefinition);
        $newFieldRevisionTable = $tableMapping->getDedicatedRevisionTableName($newFieldStorageDefinition);
        $fieldTablesMappingArray[$oldFieldRevisionTable] = $newFieldRevisionTable;
      }
      elseif ($tableMapping->allowsSharedTableStorage($oldFieldStorageDefinition)) {
        $revision_table = $entityType->getRevisionDataTable() ?: $entityType->getRevisionTable();
        // If shared table storage is allowed, we simply use the same table name
        // as key and value:
        $fieldTablesMappingArray[$revision_table] = $revision_table;
      }
    }

    return $fieldTablesMappingArray;
  }

  /**
   * Converts field table data to be used by the new field.
   *
   * @param array $fieldTablesMappingArray
   *   The mapping array that defines the relationship between field tables.
   * @param string $oldFieldName
   *   The name of the old field.
   * @param string $newFieldName
   *   The name of the new field.
   */
  protected function convertFieldTableData($fieldTablesMappingArray, $oldFieldName, $newFieldName) {
    $oldFieldTableData = [];
    foreach ($fieldTablesMappingArray as $oldFieldTable => $newFieldTable) {
      // Get the old data.
      $oldFieldTableData[$oldFieldTable] = $this->database->select($oldFieldTable)
        ->fields($oldFieldTable)
        ->execute()
        ->fetchAll(\PDO::FETCH_ASSOC);
    }

    // Adjust old field table data to conform with the new field name:
    $convertedFieldTableData = [];
    foreach ($oldFieldTableData as $oldFieldTable => $fieldTableContents) {
      $convertedFieldTableContents = [];
      foreach ($fieldTableContents as $rowKey => $rowData) {
        foreach ($rowData as $tableColumnKey => $tableValue) {
          // If a table column contains the old field name, replace it with the
          // new field name, otherwise, use the column name as is:
          if (str_contains($tableColumnKey, $oldFieldName)) {
            $newTableColumnKey = str_replace($oldFieldName, $newFieldName, $tableColumnKey);
            $convertedFieldTableContents[$rowKey][$newTableColumnKey] = $tableValue;
          }
          else {
            $convertedFieldTableContents[$rowKey][$tableColumnKey] = $tableValue;
          }
        }
      }
      // The old field table names are resolved to the new field table
      // names through our "$fieldTablesMappingArray" variable here. Meaning
      // "$fieldTablesMappingArray[$oldFieldTable]" will resolve to the new
      // field table name:
      $convertedFieldTableData[$fieldTablesMappingArray[$oldFieldTable]] = $convertedFieldTableContents;
    }

    return $convertedFieldTableData;
  }

  /**
   * Copies the converted field table data to the new field table.
   *
   * @param array $convertedFieldTableData
   *   The converted field table data.
   */
  protected function copyFieldTableData(array $convertedFieldTableData) {
    foreach ($convertedFieldTableData as $newFieldTableName => $tableData) {
      if (!empty($tableData)) {
        $insertQuery = $this->database
          ->insert($newFieldTableName)
            // Define the columns to insert values into:
          ->fields(array_keys(end($tableData)));
        foreach ($tableData as $row) {
          // Insert each table row:
          $insertQuery->values(array_values($row));
        }
        $insertQuery->execute();
      }
    }
  }

  /**
   * Changes the source configuration on existing media types.
   *
   * If the converted field is used as a source field in the source
   * configuration.
   *
   * @param string $oldFieldName
   *   The old field name.
   * @param string $newFieldName
   *   The new field name.
   */
  protected function changeSourceConfigurationOnExistingMediaTypes(string $oldFieldName, string $newFieldName) {
    // Load the old configs and media.type configs and update the
    // "source_configuration" key.
    $mediaTypeConfigs = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    foreach ($mediaTypeConfigs as $mediaTypeConfig) {
      $sourceConfiguration = $mediaTypeConfig->get('source_configuration');
      if (empty($sourceConfiguration)) {
        // No source configuration, skip this media type.
        continue;
      }
      if ($sourceConfiguration['source_field'] === $oldFieldName) {
        $sourceConfiguration['source_field'] = $newFieldName;
        $mediaTypeConfig->set('source_configuration', $sourceConfiguration);
        $mediaTypeConfig->save();
      }
    }
  }

}
